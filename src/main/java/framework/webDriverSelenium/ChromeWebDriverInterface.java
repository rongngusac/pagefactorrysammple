package framework.webDriverSelenium;

import org.openqa.selenium.WebElement;

public interface ChromeWebDriverInterface {

    public WebElement isElementPresent(String element);

    public void inputControl(String element, String value);
}

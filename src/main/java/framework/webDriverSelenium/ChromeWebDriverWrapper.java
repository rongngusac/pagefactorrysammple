package framework.webDriverSelenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChromeWebDriverWrapper implements ChromeWebDriverInterface {
    public WebDriver webDriver;

    public ChromeWebDriverWrapper(WebDriver driver) {
        this.webDriver = driver;
        //TODO: define webDriver variable to use for whole class
            //TODO: should call getWebDriver when declaring ChromeWebDriverWrapper to get webDriver
                //TODO: 1. Define WebDriver as a parameter then input from outside
                //TODO: 2. Get WebDriver within this class (trying)
    }

    @Override
    public WebElement isElementPresent(String element) {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 20);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
        WebElement webElement = webDriver.findElement(By.xpath(element));

        return webElement;
    }

    @Override
    public void inputControl(String element, String value) {
        WebElement webElement = isElementPresent(element);
        webElement.sendKeys(value);
    }
}

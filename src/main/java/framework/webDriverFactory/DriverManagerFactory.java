package framework.webDriverFactory;

public class DriverManagerFactory {

    public DriverManagerFactory() {

    }
    public static DriverManager getDriverManager(String driverType) {
        DriverManager driverManager;
        switch (driverType) {
            case "CHROME":
                driverManager = new ChromeDriverManager();
                break;
            default:
                driverManager = new ChromeDriverManager();
                System.out.println("No called browser");
                break;
        }
        return driverManager;
    }
}

package testcases;

import framework.webDriverFactory.DriverManager;
import framework.webDriverFactory.DriverManagerFactory;
import framework.webDriverSelenium.ChromeWebDriverWrapper;
import org.openqa.selenium.WebDriver;


public class TestCaseBase {
    DriverManager driverManager;
    WebDriver webDriver;
    public ChromeWebDriverWrapper chromeWebDriverWrapper;
    public String uTestURL = "https://www.utest.com/signup/personal";

    public void navigateToUTest() {
        driverManager = DriverManagerFactory.getDriverManager("CHROME");
        webDriver = driverManager.getWebDriver();
        webDriver.get(uTestURL);
    }
}

package testcases.SignIn;

import framework.webDriverFactory.DriverManager;
import framework.webDriverFactory.DriverManagerFactory;
import framework.webDriverSelenium.ChromeWebDriverWrapper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObject.SignIn.LoginPage;
import testcases.TestCaseBase;
import testcases.UiTester;

import java.net.URL;

import static framework.webDriverFactory.SessionWebDriver.createDriverFromSession;

public class LoginPageTest extends TestCaseBase {
    DriverManager driverManager;
    WebDriver driver;

    @BeforeClass
    public void setUp() {
//        driverManager = DriverManagerFactory.getDriverManager("CHROME");
//        driver = driverManager.getWebDriver();
//        driver.get(uTestURL);
//        driver.manage().window().maximize();
        WebDriverManager.chromedriver().setup();

        ChromeDriver chromeDriver = new ChromeDriver();

        HttpCommandExecutor executor = (HttpCommandExecutor) chromeDriver.getCommandExecutor();
        URL url = executor.getAddressOfRemoteServer();
        SessionId sessionId = chromeDriver.getSessionId();

        RemoteWebDriver driver2 = createDriverFromSession(sessionId, url);
        driver2.get(uTestURL);
        driver2.manage().window().maximize();
    }

    @Test
    public void inputFirstName() {
        //UiTester.loginPage.inputEmailTextBox("Long Nguyen");
    }
}

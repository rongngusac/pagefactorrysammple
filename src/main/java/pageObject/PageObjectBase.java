package pageObject;

import framework.webDriverFactory.DriverManager;
import framework.webDriverFactory.DriverManagerFactory;
import framework.webDriverSelenium.ChromeWebDriverWrapper;
import org.openqa.selenium.WebDriver;

public class PageObjectBase {

    public DriverManager driverManager = DriverManagerFactory.getDriverManager("CHROME");
    public WebDriver driver = driverManager.getWebDriver();
    public ChromeWebDriverWrapper chromeWebDriverWrapper = new ChromeWebDriverWrapper(driver);

}

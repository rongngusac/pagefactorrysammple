package pageObject.SignIn;

import pageObject.PageObjectBase;

public class LoginPage extends PageObjectBase {
    public LoginPage(){}

    private String emailTextBox = "//input[@id='firstName']";

    public void inputEmailTextBox(String email) {
        chromeWebDriverWrapper.isElementPresent(emailTextBox);
        chromeWebDriverWrapper.inputControl(emailTextBox, email);
    }
}
